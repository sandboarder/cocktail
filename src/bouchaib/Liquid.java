package bouchaib;

public class Liquid {
    private String nameProduct;
    private Category category;

    public Liquid(Category category) {
        this.category = category;
    }

    public Liquid(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public Category getCategory() {
        return category;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
